(function ($) {
  $(document).ready(function () {
    $(window).scroll(function() {
      if ($(document).scrollTop() > 250) $('body').addClass('scrolled');  
      else $('body').removeClass('scrolled');
    });

    $('.siobhan-unsplash').click(function() { 
      $('.siobhan-splash').css('display', 'none');
    });

    $('.siobhan-scroll-to-top').click(function() { 
      $('html, body').animate({scrollTop : 0},800);
    });

    $('.siobhan-toggle.left').click(function () {
      $('#siobhan-oc-left').css({ side : "inherit" });
      if($('#siobhan-oc-right').hasClass('shown')) siobhanOcHide('right');
      siobhanOcToggle('left');
    });
    $('.siobhan-toggle.right').click(function () {
      if($('#siobhan-oc-left').hasClass('shown')) siobhanOcHide('left'); 
      siobhanOcToggle('right');
    });

    $('.siobhan-overlay').click(function () {
      siobhanOcHide('left');
      siobhanOcHide('right');
    });

    function siobhanOcShow(side) {
      $('#siobhan-oc-'+side).addClass('shown');
      $('body').addClass('siobhan-oc-shown');
    }

    function siobhanOcHide(side) {
      $('#siobhan-oc-'+side).removeClass('shown');
      $('body').removeClass('siobhan-oc-shown');
    }

    function siobhanOcToggle(side) {
      if($('#siobhan-oc-'+side).hasClass('shown')) siobhanOcHide(side); 
      else siobhanOcShow(side); 
    }

    $('.siobhan-oc-panel').on('flick', function(e) {
      var side = $(this).attr('data-side');
      var direction = e['direction'];
      if(side == 'right') direction = direction * -1; 
      if(e['orientation'] == 'horizontal' && direction == -1) siobhanOcHide(side);
    });

    $('.siobhan-oc-panel').on('drag', function(e) {
      panel = $(this);
      var side = panel.attr('data-side');
      var offset = e['dx'];
      if(side == 'right')  offset = offset * -1;
      var dist = offset - 300;
      if(e['end']) {
        if(dist<-400) { 
          siobhanOcHide(side);
          setTimeout(function(){ panel.css(side,"-300px")}, 750);
        } else panel.css(side,"-300px");
      } else if(dist<-300) panel.css(side,dist+"px");
    });
  });
}(jQuery));
